__entryscape_config = {
    staticBuildVersion: "1.3.1",
    theme: {
        appName: "", //Part of logo
        logoFileName: "logo.png",
        oneRowNavbar: false,
        localTheme: true
    },
    entrystore: {
        repository: "https://goteborg.entryscape.net/store/"
    },
  itemstore: {
    bundles: [
      "templates/dc/dc",
      "templates/rdfs/rdfs",
      "templates/schema.org/schema",
      "templates/org/org",
    ]
  },
  site: {
        "!moduleList": ["terms", "workbench", "admin"]
  },
  "entitytypes": {
    "organization": {
      label: {en: "Organization", sv: "Organisation"},
      rdfType: "http://www.w3.org/ns/org#Organization",
      template: "org:Organization",
      templateLevel: "recommended",
      includeInternal: true,
      createDialog: true,
      importDialog: false
    },
    "organizationunit": {
      label: {en: "Unit", sv: "Enhet"},
      rdfType: "http://www.w3.org/ns/org#OrganizationalUnit",
      template: "org:OrganizationalUnit",
      templateLevel: "recommended",
      includeInternal: true,
      createDialog: true,
      importDialog: false
    },
    "site": {
      label: {en: "Site", sv: "Plats"},
      rdfType: "http://www.w3.org/ns/org#Site",
      template: "org:Site",
      templateLevel: "recommended",
      includeInternal: true,
      inlineCreation: true,
      createDialog: true,
      importDialog: false
    },
    "organizationperson": {
      label: {en: "Person", sv: "Person"},
      rdfType: "http://xmlns.com/foaf/0.1/Person",
      template: "org:Person",
      templateLevel: "recommended",
      includeInternal: true,
      createDialog: true,
      importDialog: false
    },
    "roll": {
      label: {en: "Roll", sv: "Roll"},
      rdfType: "http://www.w3.org/ns/org#Membership",
      template: "org:Membership",
      templateLevel: "recommended",
      includeInternal: true,
      createDialog: true,
      importDialog: false
    }
  },
  reCaptchaSiteKey: "6LeraBITAAAAAETQ_-wpGZOJ7a9jKRpF1g8OYc2O"
};
